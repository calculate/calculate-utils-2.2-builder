#!/sbin/runscript

depend() {
	need modules
	before cpufrequtils
}

KERNEL_VERSION=`uname -r`
KERNEL_VERSION_CONF=
MODULES_PATH="/lib/modules/${KERNEL_VERSION}"
CONF_FILE="/etc/modules.load.d/cpufreq.conf"
# get variable value from cl-kernel (lib variable)
variable_value()
{
	local varname=$1
	cl-kernel -v --filter $varname | 
		awk "{ if (\$1 == \"$varname\") print \$3 }"
}

# detect modules for cpu freq
get_cpufreq_modules(){
	MODULES_STRING=""
	# find all cpufreq modules 
	for path in $(find $MODULES_PATH -name cpufreq)
	do
		# load each module and chech it
		for module_path in $(find $path -name *.ko | sort)
		do
			module_name=${module_path##\/*\/}
			module_name=${module_name%.ko}
			if modprobe $module_name &>/dev/null;
			then
				if [[ ${MODULES_STRING} ]];
				then
					MODULES_STRING="${MODULES_STRING}\n$module_name"
				else
					MODULES_STRING=$module_name
				fi
			fi
		done
	done
}

# add cpu freq modules to modules autoload file
add_autoload_modules(){
	# get root type
	local roottype=`variable_value os_root_type`
	ebegin "Added rules for cpu frequency"
	# detect cpufreq modules
	get_cpufreq_modules
	# if root type is hdd
	if [[ $roottype == "hdd" ]]
	then
		# append config string to conf file
		[ -d /etc/modules.load.d ]] || mkdir /etc/modules.load.d
		echo -e "${MODULES_STRING}" > ${CONF_FILE}
	fi
}

start() {
	add_autoload_modules
	eend
}

stop() {
	# remove calculate-builder from autorun
	local roottype=`variable_value os_root_type`
	if [[ $roottype == "hdd" ]] 
	then
		for runlevel in boot default
		do
			if rc-config list $runlevel | grep -q calculate-builder
			then
				rc-update del calculate-builder $runlevel
			fi
		done
	fi
}
