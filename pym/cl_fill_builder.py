#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import re
import cl_overriding
from cl_vars_share import varsShare
from cl_kernel import cl_kernel
from os import access, R_OK,W_OK
from os import path
from cl_distr import Distributive
from cl_utils import getTupleVersion,genpassword,pathJoin,_toUNICODE, \
                     getFilesCount,getCmdLineParam,countFiles,listDirectory
from operator import itemgetter
from types import ListType
from cl_datavars import iniParser
from cl_vars import Data as libData
from datetime import datetime

class fillVars(object, varsShare):

    def get_cl_kernel_src_path(self):
        defaultKernelPath = '/usr/src'
        priorityDir = path.join(defaultKernelPath,"linux")
        if not access(defaultKernelPath,R_OK):
            return ""
        kernel = cl_kernel()
        kernelDirs = filter(kernel._testKernelDirectory,
                            map(lambda x:path.join(defaultKernelPath,x),
                                os.listdir(defaultKernelPath)))
        if kernelDirs:
            if priorityDir in kernelDirs:
                return priorityDir
            else:
                return path.join(defaultKernelPath,kernelDirs[0])
        return ""

    def get_cl_kernel_full_ver(self):
        if self.Get('cl_kernel_config') \
            and os.access(self.Get('cl_kernel_config'),os.R_OK):
            localVer = self.getValueFromConfig(
                        self.Get('cl_kernel_config'),
                                    'CONFIG_LOCALVERSION')
            if not localVer:
                localVer=""
        else:
            localVer = ""
        kernelMakefile = path.join(self.Get('cl_kernel_src_path'),'Makefile')
        if os.access(kernelMakefile,os.R_OK):
            extraVer = ""
            for line in open(kernelMakefile,'r'):
                if "EXTRAVERSION" in line:
                    extraVer = line.partition('=')[2].strip()
                    break
        else:
            localVer = ""
        return "%s%s%s"%(self.Get('cl_kernel_ver'), extraVer, localVer)

    def get_cl_builder_kernel_suffix(self):
        kernelname = self.Get('cl_builder_kernel')
        reShortname = re.compile(
            r"^vmlinuz-((?:\d+\.)*\d+)-(x86_64|i686)-(.*)$",re.S)
        res = reShortname.search(kernelname)
        bootPath = path.join(self.Get('cl_builder_path'),'boot')
        if res:
            return "%s-%s-%s"%(res.groups()[0],
                               self.Get('os_builder_arch_machine'),
                               self.Get('os_builder_linux_shortname'))
        return ""


    def get_cl_kernel_suffix(self):
        kernelFullVer = self.Get('cl_kernel_full_ver')
        fullVerWithoutCalculate = kernelFullVer.replace("-calculate","")
        return "%s-%s-%s"%(fullVerWithoutCalculate,
                                           self.Get('os_arch_machine'),
                                           self.Get('os_linux_shortname'))

    def get_cl_kernel_ver(self):
        kernelMakefile = path.join(self.Get('cl_kernel_src_path'),'Makefile')
        reVerPart = re.compile(
            "^(?:VERSION|^PATCHLEVEL|^SUBLEVEL)\s*=",re.I)
        if access(kernelMakefile,R_OK):
            try:
                makeVer = \
                  "%(VERSION)s.%(PATCHLEVEL)s.%(SUBLEVEL)s"% \
                    dict(map(lambda x:(x[0].strip(),x[2].strip()),
                             map(lambda x:x.partition('='),
                                 filter(reVerPart.search,
                                        open(kernelMakefile,'r')))))
                return makeVer
            except:
                pass
        return ""

    def get_cl_kernel_config(self):
        kernelDirs = self.Get('cl_kernel_config_path')
        if not access(kernelDirs,R_OK):
            return ""
        # check first number (3 or other kernel)
        tupleKernelVer = self.Get('cl_kernel_ver').split('.')
        if tupleKernelVer and tupleKernelVer[0] == '3':
            shortVerSearch = re.search("^\d+\.\d+",
                self.Get('cl_kernel_ver'),re.I)
        else:
            shortVerSearch = re.search("^\d+\.\d+.\d+",
                                       self.Get('cl_kernel_ver'),re.I)
        if shortVerSearch:
            shortVer = shortVerSearch.group()
        else:
            return ""
        # find best config
        makeProfile = '/etc/make.profile'
        profileSpec = ''
        if path.islink(makeProfile):
            linkTarget = os.readlink('/etc/make.profile')
            parentDir, profileSpecification = path.split(linkTarget)
            if parentDir.endswith('x86') or parentDir.endswith('amd64'):
                profileSpec = profileSpecification
        configName = "config-%(system)s-%(march)s-%(ver)s" % \
                {'system':self.Get('os_linux_system'),
                 'march':self.Get('os_arch_machine'),
                 'ver':shortVer}
        configNameSpec = "config-%(system)s-%(march)s-%(ver)s-%(spec)s" % \
                {'system':self.Get('os_linux_system'),
                 'march':self.Get('os_arch_machine'),
                 'ver':shortVer,
                 'spec':profileSpec}
        if profileSpec and path.exists(path.join(kernelDirs,configNameSpec)):
            return path.join(kernelDirs,configNameSpec)
        elif path.exists(path.join(kernelDirs,configName)):
            return path.join(kernelDirs,configName)
        return ""

    def get_os_builder_makeopts(self):
        makeconf = '/etc/make.conf'
        if access(makeconf,R_OK):
            makeopts = self.getValueFromConfig('/etc/make.conf',"MAKEOPTS")
            if makeopts:
                return makeopts
        return ""

    def get_cl_kernel_boot_path(self):
        return path.join(self.Get('cl_kernel_install_path'),"boot")

    def get_cl_kernel_cache_path(self):
        cacheDir = self.getValueFromConfig('/etc/genkernel.conf','CACHE_DIR')
        if cacheDir == False:
            return "/var/cache/genkernel"
        else:
            return cacheDir

    def get_ac_builder_iso(self):
        """Need perform templates for builder:iso"""
        if self.Get('cl_action')=='iso':
            return "up"
        return ""

    def get_ac_builder_squash(self):
        """Need perform templates for builder:squahs"""
        if self.Get('cl_action')=='squash':
            return "up"
        return ""

    def get_cl_builder_path(self):
        """Get path for image"""
        paths = map(lambda x:x[1],reversed(libData.cl_env_data['value']))
        if self.Get('cl_action') != 'kernel':
            for inifile in paths:
                if os.access(inifile,os.R_OK):
                    inidata = iniParser(inifile)
                    res = inidata.getVar("assemble","cl_assemble_path",
                                         checkExistVar=True)
                    if res and res[0]:
                        return _toUNICODE(res[1]).encode('utf-8')
            return "/mnt/builder"
        return "/"

    def get_cl_builder_kernel(self):
        """Kernel image"""
        bootdir = path.join(self.Get('cl_builder_path'),'boot')
        modulesdir = path.join(self.Get('cl_builder_path'),'lib/modules')
        if not os.access(modulesdir,os.R_OK):
            return ""
        validKernel = os.listdir(modulesdir)
        kernelFiles = self.getFilesByType(bootdir,"Linux kernel")
        kernelsWithVer = \
            map(lambda x:(x[0],(getTupleVersion("".join(x[1].groups()[0:3:2])),
                                path.getmtime(x[0]))),
                filter(lambda x:x[1] and x[1].group() in validKernel,
                       map(lambda x:(x[0],self.reFindVer.search(x[1])),
                       kernelFiles)))
        if kernelsWithVer:
            return path.split(max(kernelsWithVer,key=itemgetter(1))[0])[-1]
        return ""

    def get_cl_builder_initrd_install(self):
        kernelname = self.Get('cl_builder_kernel')
        reShortname = re.compile(
            r"^vmlinuz-(\d+\.)*\d+-(x86_64|i686)-(.*)$",re.S)
        res = reShortname.search(kernelname)
        if res:
            shortname = res.groups()[2]
            archmachine = res.groups()[1]
        else:
            shortname = self.Get('os_builder_linux_shortname')
            archmachine = self.Get('os_builder_arch_machine')
        return self.getInitrd(archmachine,
                              shortname,
                              self.Get('cl_builder_path'),
                              kernelname,
                              suffix="install") or ""

    def get_cl_builder_kernel_config(self):
        kernelname = self.Get('cl_builder_kernel')
        reShortname = re.compile(
            r"^vmlinuz-((?:\d+\.)*\d+)-(x86_64|i686)-(.*)$",re.S)
        res = reShortname.search(kernelname)
        bootPath = path.join(self.Get('cl_builder_path'),'boot')
        if res:
            kernelconfig = "config-%s-%s-%s"%(res.groups()[:3])
            if path.exists(path.join(bootPath,kernelconfig)):
                return kernelconfig
        elif path.exists(bootPath):
            configs = filter(lambda x:x.startswith('config'),
                             os.listdir(bootPath))
            if configs:
                return configs[0]
        return ""

    def get_cl_builder_kernel_systemmap(self):
        kernelname = self.Get('cl_builder_kernel')
        reShortname = re.compile(
            r"^vmlinuz-((?:\d+\.)*\d+)-(x86_64|i686)-(.*)$",re.S)
        res = reShortname.search(kernelname)
        bootPath = path.join(self.Get('cl_builder_path'),'boot')
        if res:
            systemmap = "System.map-%s-%s-%s"%(res.groups()[:3])
            if path.exists(path.join(bootPath,systemmap)):
                return systemmap
        elif path.exists(bootPath):
            systemmaps = filter(lambda x:x.startswith('System.map'),
                             os.listdir(bootPath))
            if systemmaps:
                return systemmaps[0]
        return ""

    def get_os_builder_linux_name(self):
        """Build system name"""
        linuxShortName = self.Get("os_builder_linux_shortname")
        return self.dictLinuxName.get(linuxShortName,"Linux")

    def get_os_builder_linux_subname(self):
        """Build system subname"""
        linuxShortName = self.Get("os_builder_linux_shortname")
        return self.dictLinuxSubName.get(linuxShortName,"")

    def get_os_builder_linux_shortname(self):
        """Build system shortname"""
        systemRoot = self.Get('cl_builder_path')
        return self.getShortnameByMakeprofile(systemRoot) or \
               self.getShortnameByIni(systemRoot) or \
               self.detectOtherShortname(systemRoot) or \
               "Linux"

    def get_os_builder_linux_filesnum(self):
        """Files number in image system"""
        if self.Get('cl_builder_tree') == 'off':
            sourceDirectory = self.Get('cl_builder_path')
            portagePath = pathJoin(sourceDirectory,"usr/portage")
            overlayPath = pathJoin(sourceDirectory,"var/lib/layman/calculate")
            excludeCount = \
                  reduce(lambda x,y:x + y,
                  map(countFiles,
                  map(lambda x: path.join(sourceDirectory,"var/lib/layman/calculate",x),
                  filter(lambda x: not x in ("distfiles", "eclass", ".git","profiles"),
                  listDirectory(overlayPath))) + \
                  map(lambda x: path.join(sourceDirectory,"usr/portage",x),
                  filter(lambda x: not x in ("distfiles", "eclass", ".git","profiles"),
                  listDirectory(portagePath)))),0)
        else:
            excludeCount = 0
        systemRoot = self.Get('cl_builder_path')
        return str(countFiles(systemRoot)-excludeCount)

    def getVersionFromCalculateEnv(self,systemroot):
        """Get version from calculate ini"""
        pathname = path.join(systemroot,
                      'var/lib/layman/calculate/profiles/calculate.env')
        if path.exists(pathname):
            FD = open(pathname)
            data = FD.readlines()
            FD.close()
            verList = filter(lambda y:y,
                                   map(lambda x:\
                                       len(x.split("="))==2 and\
                                       x.split("=")[0].strip() == "os_linux_ver" and\
                                       x.split("=")[1].strip(), data))
            if verList:
                reVer=re.compile("^((\d+\.)*\d+|\d+)$",re.S)
                reRes = filter(reVer.search,verList)
                if reRes:
                    return reRes[0]

    def get_os_builder_linux_ver(self):
        """Build system ver"""
        linuxShortName = self.Get("os_builder_linux_shortname")
        rootSystem = self.Get('cl_builder_path')
        return self.getVersionFromMetapackage(rootSystem,linuxShortName) or \
            self.getVersionFromCalculateEnv(rootSystem) or \
            self.getVersionFromCalculateIni(rootSystem) or \
            self.getVersionFromGentooFiles(rootSystem) or "0"

    def get_os_builder_arch_machine(self):
        """Build system arch"""
        rootSystem = self.Get('cl_builder_path')
        lib64path = path.join(rootSystem,"lib64")
        return 'x86_64' if path.lexists(lib64path) else "i686"

    def get_os_builder_linux_system(self):
        """Build linux system type"""
        shortName = self.Get('os_builder_linux_shortname')
        return self.dictNameSystem.get(shortName,"")

    def get_cl_builder_cdname(self):
        """Cd size specified by name (DVD/CD)"""
        squashfile = pathJoin(self.Get('cl_builder_iso_path'),
                               self.Get('cl_builder_current_squash'))
        kernelfile = pathJoin(self.Get('cl_builder_iso_path'),
                              self.Get('cl_builder_squash_path'),
                              'boot',
                              self.Get('cl_builder_kernel'))
        initrdfile = pathJoin(self.Get('cl_builder_iso_path'),
                              self.Get('cl_builder_squash_path'),
                              'boot',
                              self.Get('cl_builder_initrd_install'))
        if os.access(squashfile,R_OK) and os.access(kernelfile,R_OK) and \
            os.access(initrdfile,R_OK):
            isosize = path.getsize(squashfile)+path.getsize(kernelfile)+ \
                        path.getsize(initrdfile)+2*1024*1024
            if isosize > 700*1024*1024:
                return "DVD"
            else:
                return "CD"
        return ""

    def getSquashNum(self,reMatch):
        if reMatch.groups()[1] and reMatch.groups()[1].isdigit():
            return int(reMatch.groups()[1])
        else:
            return 0

    def get_cl_builder_remove_squash(self):
        """Livecd.squashfs name for del(may be livecd.squashfs.2 and etc)"""
        directory = self.Get('cl_builder_iso_path')
        if not os.access(directory,R_OK):
            return ""
        squashfiles = filter(lambda x:x,
                             map(Distributive.reLive.search,
                                 os.listdir(directory)))
        curName = self.Get('cl_builder_old_squash')
        if squashfiles:
            return  filter(lambda x:x != curName,
                    map(lambda x:x.group(),
                    sorted(squashfiles, key=self.getSquashNum)))
        return ""

    def get_cl_builder_current_squash(self):
        """
        Variable keep value of squash image which will created by squash command
        """
        oldSquash = self.Get('cl_builder_old_squash')
        reOldSquash = Distributive.reLive.search(oldSquash)
        if "/mnt/flash" in self.Get('cl_builder_iso_path') and reOldSquash:
            newNum = self.getSquashNum(reOldSquash)+1
            return "livecd.squashfs.%d"%newNum
        else:
            return "livecd.squashfs"

    def get_cl_builder_old_squash(self):
        """
        Variable keep value of boot squash image
        """
        loopValue = getCmdLineParam('loop')
        if loopValue:
            return path.basename(loopValue)
        return ""

    def get_cl_builder_iso_path(self):
        directory = \
            "/var/calculate/tmp/iso-%s-%s"%(self.Get('os_builder_linux_shortname'),
                                            self.Get('os_builder_arch_machine'))
        newDirectoryName = directory
        while path.exists(newDirectoryName):
            newDirectoryName = "%s.%s"%(directory,genpassword(8))
        return newDirectoryName

    def get_cl_builder_image(self):
        """Iso image full path"""
        imagepath = self.Get('cl_builder_image_path')
        if type(imagepath) != ListType:
            imagepath = [imagepath]
        paths = filter(lambda x:os.access(x,W_OK), imagepath)
        if paths:
            return path.join(paths[0],"%(shortname)s-%(ver)s-%(march)s.iso"%
                 {'shortname':self.Get('os_builder_linux_shortname').lower(),
                  'ver':self.Get('os_builder_linux_build') or 
                        self.Get('os_builder_linux_ver'),
                  'march':self.Get('os_builder_arch_machine')})
        return ""

    def get_os_builder_linux_build(self):
        """Generate build"""
        curdate = datetime.now()
        return "%04d%02d%02d"%(curdate.year,curdate.month,curdate.day)

    def get_cl_builder_distro(self):
        """Current assembling systems"""
        envFile = '/etc/calculate/assemble.env'
        envData = iniParser(envFile)
        return filter(lambda x:envData.getVar(x,
                              'os_assemble_root_dev'),
            map(lambda x:_toUNICODE(x).encode('utf-8'),
                   envData.getAllSectionNames()))

    def get_cl_builder_filesnum(self):
        """Get files num in assembled distro"""
        #getFilesCount(self.Get('cl_builder_path'))
        return "0"

    def get_cl_builder_image_path(self):
        """Path which contains images"""
        imagePath = ['/var/calculate/linux',
                  '/var/calculate/remote/linux',
                  '/usr/calculate/share/linux']
        if self.Get('os_builder_profile'):
            imagePath.insert(0,path.join("/var/calculate/remote/assemble",
                     self.Get('os_builder_profile').replace('/','-'),
                     "linux"))
        return imagePath

    def get_cl_builder_kernel_cmd(self):
        """Suffix for kernel cmdline"""
        suffix = ""
        timezone = self.Get('os_builder_clock_timezone')
        lang =     self.Get('os_builder_locale_lang') 
        if timezone and lang:
            suffix = "calculate=lang:%s,keymap:%s,timezone:%s" % \
                (lang,lang,timezone)
        elif timezone:
            suffix = "calculate=,,timezone:%s"%(timezone)
        elif lang:
            suffix = "calculate=lang:%s,keymap:%s" % (lang,lang)
        print suffix
        return suffix
