#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0 #
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Допустимые ключи значений
#   mode -     read only or writeable variable
#   value    - default variable value
#   select   - list of posible values for variable
#   hide - flag, if it is True, then the variable is not printable
#   printval - print value of variable
from cl_builder import __version__, __app__

class Data:
    # relative path for apply templates on files of system
    cl_root_path = {}

    # program name
    cl_name = {'value':__app__}

    # program version
    cl_ver = {'value':__version__}

    # kernel sources path
    cl_kernel_src_path = {}

    # kernel config path
    cl_kernel_config = {}

    # path which has configs
    cl_kernel_config_path = {'mode':'w',
                    'value':'/var/lib/layman/calculate/profiles/kernel'}

    # suffix of kernel
    cl_kernel_suffix = {}

    # kernel version
    cl_kernel_ver = {}

    # kernel full version (added extraversion and localversion)
    cl_kernel_full_ver = {}

    # make opts received from make.conf
    os_builder_makeopts = {'mode':'w'}

    # path for vmlinuz,initramfs,system.map and config
    cl_kernel_boot_path = {'mode':'w'}

    # root path for kernel installation (value+lib/modules)
    cl_kernel_install_path = {'mode':'w','value':'/'}

    # override location of Calckernel's temporary directory
    cl_kernel_temp_path = {'mode':'w','value':'/var/calculate/tmp/genkernel'}

    # override the default cache location for calckernel
    cl_kernel_cache_path = {'mode':'w'}

    # need perform templates for builder:iso
    ac_builder_iso = {}

    # need perform templates for builder:squahs
    ac_builder_squash = {}

    # directory for packing
    cl_builder_path = {'mode':'w',
                       'value':'/mnt/builder'}

    # directory of system for livecd.squashfs
    cl_builder_squash_path = {'mode':'w',
                              'value':"/livecd.squashfs.dir"}

    # directory for iso building
    cl_builder_iso_path = {'mode':'w'}

    # kernel image
    cl_builder_kernel = {}

    # initramfs image
    cl_builder_initrd_install = {}

    # kernel config name
    cl_builder_kernel_config = {}

    # kernel system.map
    cl_builder_kernel_systemmap = {}

    # kernel suffix for builder
    cl_builder_kernel_suffix = {}

    # include portage directory from image
    cl_builder_tree = {'value':'off'}

    cl_builder_isohybrid_set = {'value':'on'}

    # build system name
    os_builder_linux_name = {}

    # build system subname
    os_builder_linux_subname = {}

    # build system shortname
    os_builder_linux_shortname = {}

    # build system ver
    os_builder_linux_ver = {'mode':'r'}

    # build system build
    os_builder_linux_build = {'mode':'r','value':''}

    # filesnum in build system
    os_builder_linux_filesnum = {'mode':'r'}

    # build system arch
    os_builder_arch_machine = {}

    # build linux system type
    os_builder_linux_system = {}

    # cd size specified by name (DVD/CD)
    cl_builder_cdname = {}

    """
    Variable keep value of squash image which will created by squash command
    """
    cl_builder_current_squash = {}

    """
    Variable keep value of boot squash image
    """
    cl_builder_old_squash = {}

    # livecd.squashfs name for remove (may be livecd.squashfs.2 and etc)
    cl_builder_remove_squash = {}

    # iso image full path
    cl_builder_image = {}

    # distro created by assemble
    cl_builder_distro = {}

    os_builder_profile = {}

    # count of files and directories in distributive
    cl_builder_filesnum = {}

    # path which contains images
    cl_builder_image_path = {}

    cl_builder_live_set = {'value':'off'}

    os_builder_locale_lang = {'mode':'w','value':''}

    os_builder_clock_timezone = {'mode':'w','value':''}

    cl_builder_kernel_cmd = {'mode':'w'}

    # lib vars
    cl_chroot_path = {}
    cl_env_path = {}
    cl_kernel_uid = {}
    os_arch_machine  = {}
    os_linux_shortname = {}
    os_linux_system = {}
    os_root_dev = {}
    os_root_type = {}
    os_scratch = {}
    cl_chroot_status = {}
