#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-client

#Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import stat
from distutils.core import setup
from distutils.command.install_data import install_data


__app__ = "calculate-builder"
__version__ = "2.2.31"

data_files = []

var_data_files = []

def __scanDir(scanDir, prefix, replace_dirname, dirData, flagDir=False):
    """Scan directory"""
    files = []
    dirs = []
    if flagDir or stat.S_ISDIR(os.stat(scanDir)[stat.ST_MODE]):
        for fileOrDir in os.listdir(scanDir):
            absPath = os.path.join(scanDir,fileOrDir)
            statInfo = os.stat(absPath)[stat.ST_MODE]
            if stat.S_ISREG(statInfo):
                files.append(absPath)
            elif stat.S_ISDIR(statInfo):
                dirs.append(absPath)
        if replace_dirname:
            listDirs = list(scanDir.partition("/"))[1:]
            listDirs.insert(0,replace_dirname)
            scanDir = "".join(listDirs)
        if prefix:
            scanDir = os.path.join(prefix,scanDir)
        dirData.append((scanDir, files))
        for sDir in dirs:
            __scanDir(sDir, prefix, replace_dirname, dirData, True)
    return dirData

def create_data_files(data_dirs, prefix="", replace_dirname=""):
    """Create data_files"""
    data_files = []
    for data_dir in data_dirs:
        data = []
        data_files += __scanDir(data_dir, prefix, replace_dirname, data)
    return data_files


data_files += [('/usr/share/calculate/config', ['data/chroot.rc'])]

class cl_install_data(install_data):
    def run (self):
        install_data.run(self)
        data_file = []
        fileNames = map(lambda x: os.path.split(x[0])[1], data_file)
        listNames = map(lambda x: filter(lambda y: y, x[0].split("/")),data_file)
        data_find = {}
        for i in range(len(fileNames)):
            listNames[i].reverse()
            data_find[fileNames[i]] =[listNames[i],data_file[i][1]]

        for path in self.get_outputs():
            nameFile = os.path.split(path)[1]
            if nameFile in data_find.keys():
                data = data_find[nameFile][0]
                mode = data_find[nameFile][1]
                flagFound = True
                iMax = len(data)
                pathFile = path
                for i in range(iMax):
                    if data[i] != os.path.split(pathFile)[1]:
                        flagFound = False
                        break
                    pathFile = os.path.split(pathFile)[0]
                if flagFound:
                    os.chmod(path, mode)

setup(
    name = __app__,
    version = __version__,
    description = "Calculate Linux builder",
    author = "Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    package_dir = {'calculate-builder': "."},
    packages = ['calculate-builder.pym'],
    data_files = data_files,
    scripts=["./scripts/cl-kernel","./scripts/cl-builder",
             "./scripts/cl-image"],
    cmdclass={'install_data': cl_install_data},
)
